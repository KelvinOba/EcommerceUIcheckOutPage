# Project Summary
-
This is a UI checkout page for an E-commerce  Company, that will ensure that a user always has a happy purchase experience when buying 
items online.The process right from the website landing page to the success page should be user friendly and seamless. The modern online shopper has a low threshold for patience.

# PROJECT ROAD MAP
-
1.	# The landing page of the company, where different products are displayed. 
2.	Clicking a buy button any product leads to the Registration page, that's if the user is not already registered. Some users don’t like creating accounts though especially if they are going for a one-time purchase from an ecommerce company.
3.	The user is prompted to confirm order by selecting either
i.	Pay online
ii.	Pay on delivery
4.	Progress is key for any journey, there should be a progress process for the user to know how long it will take to complete the purchase, this should be very appealing to the human eye, in this case a danfo bus is used as a progress bar to show the user’s progress.
5.	At this stage the user should be heading to the success page or the online payment gateway depending on the option chosen.
6.	The online payment gateway should be easy on the eyes and simple to use to enable the user complete the process of payment, with instant validation when a wrong input is made.
7.	A success page is also very important in order to inform a user on how and when to receive the product.
8.	

# NB: PROJECT TO BE UPDATED FOR RESPONSIVENESS SOON.
-
